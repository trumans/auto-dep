import * as del from 'del';
import fs from 'fs';
import tmp, {SynchrounousResult} from 'tmp';
import {buildCLI, runWith} from './helpers/cmd';

let tempDir: SynchrounousResult;

beforeAll(async done => {
    await buildCLI();
    done();
});

beforeEach(done => {
    tempDir = tmp.dirSync();
    fs.copyFile('./e2e/fixtures/package.json', tempDir.name + '/package.json', () => {
        fs.copyFile('./e2e/fixtures/yarn.lock', tempDir.name + '/yarn.lock', () => {
            done();
        });
    });
});

afterEach(() => {
    del.sync([tempDir.name], {force: true});
});

describe('auto-dep', () => {
    it('outputs its version', async () => {
        const output = await runWith('--version');

        expect(output).toContain('0.0.2');
    });

    it('can output outdated packages', async () => {
        const output = await runWith('outdated', '--projectDir', tempDir.name);

        expect(output).toMatch(/^.*jest.*23\.6\.0/m);
    });

    it('can update all outdated packages', async () => {
        let outdatedCheck = await runWith('outdated', '--projectDir', tempDir.name);
        expect(outdatedCheck).toMatch(/^.*jest.*23\.6\.0/m);

        await runWith('update', '--projectDir', tempDir.name);

        outdatedCheck = await runWith('outdated', '--projectDir', tempDir.name);
        expect(outdatedCheck).toMatch(/^.*jest.*24\.0\.0/m);
    });
});
