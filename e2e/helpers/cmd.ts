import {spawn} from 'child_process';
import concat from 'concat-stream';

export const runWith = (...args: string[]) => {
    const childProcess = spawn('./out/cli.js', args);
    childProcess.stdin.setDefaultEncoding('utf-8');

    return new Promise((resolve, reject) => {
        childProcess.stderr.once('data', err => {
            reject(err.toString());
        });
        childProcess.on('error', e => reject(e));

        childProcess.stdout.pipe(
            concat(result => {
                resolve(result.toString());
            }),
        );
    });
};

export const buildCLI = async () => {
    const childProcess = spawn('yarn', ['build']);
    childProcess.stdin.setDefaultEncoding('utf-8');

    return new Promise((resolve, reject) => {
        childProcess.stderr.once('data', err => {
            reject(err.toString());
        });
        childProcess.on('error', e => reject(e));

        childProcess.stdout.pipe(
            concat(result => {
                resolve(result.toString());
            }),
        );
    });
};
