/* tslint:disable:max-line-length */

export const outdatedOutput: string = `yarn outdated v1.13.0
info Color legend :
 "<red>"    : Major Update backward-incompatible updates
 "<yellow>" : Minor Update backward-compatible features
 "<green>"  : Patch Update backward-compatible bug fixes
Package                                   Current Wanted Latest Package Type    URL
@babel/preset-env                         7.2.3   7.2.3  7.3.1  devDependencies https://babeljs.io/
jest                                      23.6.0  23.6.0 24.0.0 devDependencies https://jestjs.io/
✨  Done in 0.36s.`;
