import {spawnSync} from 'child_process';
import {OutdatedArgs} from './PackageManager';


export const outdated = (args: OutdatedArgs): string => {
    const result = spawnSync(
        'yarn',
        ['outdated'],
        {cwd: args.projectDir}
    );

    return result.stdout.toString();
};

export const update = (packageName: string, latest: string, workingDir: string): string => {
    const result = spawnSync(
        'yarn',
        ['upgrade', `${packageName}@${latest}`],
        {cwd: workingDir}
    );

    return result.stdout.toString();
};
