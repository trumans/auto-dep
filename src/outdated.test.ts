import {outdatedOutput} from '../e2e/fixtures/yarnOutdated';
import {outdated} from './outdated';
import {Output} from './output';
import * as Yarn from './Yarn';


jest.mock('./output');
jest.mock('./Yarn', () => {
    return {
        outdated: jest.fn()
    };
});

describe('outdated', () => {
    it('outputs the result of yarn outdated', () => {
        jest.spyOn(Yarn, 'outdated').mockReturnValue('the stdout');

        outdated();

        expect(Yarn.outdated).toHaveBeenCalledWith({projectDir: '.'});
        expect(Output.info).toHaveBeenCalledWith('the stdout');
    });

    it('accepts a project dir param', () => {
        outdated({projectDir: 'some-dir'});

        expect(Yarn.outdated).toHaveBeenCalledWith({projectDir: 'some-dir'});
    });

    it('returns a json array of outdated packages', () => {
        jest.spyOn(Yarn, 'outdated').mockReturnValue(outdatedOutput);

        const packages = outdated({projectDir: 'some-dir'});

        expect(packages).toEqual([
            {
                name: '@babel/preset-env',
                current: '7.2.3',
                latest: '7.3.1'
            },
            {
                name: 'jest',
                current: '23.6.0',
                latest: '24.0.0'
            }
        ]);
    });
});
