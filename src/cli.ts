#!/usr/bin/env node

import program, {Command} from 'commander';
import {version} from '../package.json';
import {outdated} from './outdated';
import {update} from './update';

program
    .version(version);

program
    .command('outdated')
    .option('-p, --projectDir <dir>', 'Path to directory with a package.json')
    .action((cmd: Command) => {
        outdated({projectDir: cmd.projectDir});
    });

program
    .command('update')
    .option('-p, --projectDir <dir>', 'Path to directory with a package.json')
    .action((cmd: Command) => {
        update({projectDir: cmd.projectDir});
    });

program.parse(process.argv);
