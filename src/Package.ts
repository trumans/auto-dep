export interface Package {
    name: string;
    current: string;
    latest: string;
}
