import {outdated} from './outdated';
import {Output} from './output';
import {Package} from './Package';
import {OutdatedArgs} from './PackageManager';
import * as Yarn from './Yarn';

export const update = (
    args: OutdatedArgs = {
        projectDir: '.'
    }
) => {
    outdated(args)
        .forEach((p: Package) => {
            const result = Yarn.update(p.name, p.latest, args.projectDir);
            Output.info(`updated ${p.name} from ${p.current} to ${p.latest}`);
        });
};
