import * as outdated from './outdated';
import {Output} from './output';
import {update} from './update';
import * as Yarn from './Yarn';

jest.mock('./output');
jest.mock('./outdated');
jest.mock('./Yarn');

describe('update', () => {
    it('calls to update each package', () => {
        jest.spyOn(outdated, 'outdated').mockReturnValue([
            {
                name: 'some-package',
                current: '1.2.3',
                latest: '2.3.4'
            },
            {
                name: 'some-other-package',
                current: '7.8.9',
                latest: '8.9.10'
            }
        ]);

        update();

        expect(Yarn.update).toHaveBeenCalledWith('some-package', '2.3.4', '.');
        expect(Output.info).toHaveBeenCalledWith('updated some-package from 1.2.3 to 2.3.4');

        expect(Yarn.update).toHaveBeenCalledWith('some-other-package', '8.9.10', '.');
        expect(Output.info).toHaveBeenCalledWith('updated some-other-package from 7.8.9 to 8.9.10');
    });
});
