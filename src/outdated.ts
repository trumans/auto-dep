import {Output} from './output';
import {Package} from './Package';
import {OutdatedArgs} from './PackageManager';
import * as Yarn from './Yarn';


const LINES_OF_CRUFTY_OUTPUT = 6;
const COLUMNS_IN_PACKAGE_OUTPUT = 6;

export const outdated = (
    args: OutdatedArgs = {
        projectDir: '.'
    }
): Package[] => {
    const result = Yarn.outdated(args);

    Output.info(result);

    return removeInformationalOutput(result)
        .map(convertExtraSpacesToArray)
        .filter(isPackageArray)
        .map(packageArrayToPackage);
};


const removeInformationalOutput = (result: string): string[] => result.split('\n').slice(LINES_OF_CRUFTY_OUTPUT);
const convertExtraSpacesToArray = (p: string): string[] => p.replace(/ +/g, '-_-').split('-_-');
const isPackageArray = (p: string[]): boolean => p.length === COLUMNS_IN_PACKAGE_OUTPUT;
const packageArrayToPackage = (p: string[]): Package =>  {
    return {
        name: p[0],
        current: p[1],
        latest: p[3]
    };
};
